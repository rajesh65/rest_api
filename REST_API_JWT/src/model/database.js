const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    id:{
    type: Number,
    },
    Ename:{
        type:String,
    },
    password:{
        type:String,
    },
    phone:{
        type: Number,
    }
});

module.exports = mongoose.model('User',userSchema);