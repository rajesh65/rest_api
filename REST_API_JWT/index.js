const express = require("express");

const app=express();
//const database = require("./Model/database");
const port=2016;
const mongoose = require("mongoose");
const employee= require("./src/route/Employee");
const bodyParser = require('body-parser');
const { json } = require("body-parser");
const { query } = require("express");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const authRoute = require("../REST_API_JWT/src/Authentication/auth");

//connecting db
mongoose.connect("mongodb://localhost:27017/Employee",
{useNewUrlParser:true, useUnifiedTopology: true } );
const con=mongoose.connection;

//routes...
app.use("/user",employee);

app.use("/auth",authRoute);


app.listen(port);
console.log("Server is running on port",port);