const express = require("express");
const {MongoClient}=require("mongodb");
const router = express.Router();
//let con=require('../model/database');
var url ="mongodb://localhost:27017/";

const client=new MongoClient(url,{
    useNewUrlParser:true,
    useUnifiedTopology: true
});
client.connect();

router.get("/", async (req, res) => {
    
    data=await  client.db("Emp").collection("Elist").find().toArray();
   // console.log("...",data)
    res.send(data);
});


router.post("/", async (req, res) => {
    
    const obj=req.body;
    data=await  client.db("Emp").collection("Elist").insertOne(obj);
    res.send(data);
});

router.put("/", async (req, res) => {
    var obj={ id: parseInt(req.query.id)};
    var dochange= {$set :{name:req.body.name}};
    data = await client.db("Emp").collection("Elist").updateOne(obj,dochange);
    res.send(data);
})

router.delete("/",async(req,res) =>{
 
    var obj={id: parseInt(req.query.id)};
    data =await client.db("Emp").collection("Elsit").deleteOne(obj); 
    res.send(data);
});

module.exports = router;
